# Chayka(seagull) weather station
<img src="https://gitlab.com/dm_sh/chayka-weather-station/-/raw/master/sensors_pole.jpg" width="350"> <img src="https://gitlab.com/dm_sh/chayka-weather-station/-/raw/master/cabinet.jpg" width="350">

## Location

59.923479, 30.242067 [Sevkabel Port](https://goo.gl/maps/rY5MAZhjfbWjXkwz8) 

## Sensors

* [MeteoTemp RH+T:](https://www.baranidesign.com/temperature-humidity-sensors)
    * Temperature
    * Humidity 
* [MeteoWind® Compact:](https://www.baranidesign.com/meteowind-compact)
    * Wind speed
    * Wind direction

## Controller unit

[Wiren Board 6](https://wirenboard.com/product/wiren-board-6/)

## Getting data from weather station

For data transmission [MQTT](https://mqtt.org/) protocol is used.

#### Credentials: 

`IP: chayka.shsh.io`

`Login: None`

`Password: None`

Topics available:

| Topic                      | Unit           |
| -------------              |:-------------:|
| /seagull_ws/temperature    | °C     | 
| /seagull_ws/humidity       | %      |  
| /seagull_ws/dew_point      | °C     |   
| /seagull_ws/wind_speed     | m/s    | 
| /seagull_ws/wind_direction | °     | 

mosquitto_sub — an MQTT client for subscribing to topics

Temperature example: `mosquitto_sub -h chayka.shsh.io -t /seagull_ws/temperature/+`

Get all available topics: `mosquitto_sub -h chayka.shsh.io -t "#" -v` 

### [Web interface with graphs](http://weather.shsh.io/)
